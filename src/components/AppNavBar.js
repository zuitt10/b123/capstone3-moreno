import React,{useContext} from 'react';
import {Navbar,Nav,NavDropdown,Container,Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext'
import gwapo from '../images/brand.png'
import './appnavbar.css'

export default function AppNavBar(props) {
const {countCartItems} = props
const {user} = useContext(UserContext);
  console.log(user)
	return (

		<>

	<Navbar collapseOnSelect expand="lg" bg="transparent" variant="dark">
  <Container>
  <Navbar.Brand className="navbar-brand" href="/"><img src="https://media2.giphy.com/media/6KfQuw1fqG56BQ3c3N/giphy.gif?cid=ecf05e47xyo14wygn2lx2if0dza6uu8y8frl6kutlr92y5vk&rid=giphy.gif&ct=s" className="img-fluid" /></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto" className="log">
      <Nav.Link as={Link} to="/products" >Products</Nav.Link>
      {/*<Nav.Link as={Link} to="/cart">Cart</Nav.Link>*/}
      {
          user.id
          ? 
              user.isAdmin
              ?
              <div >
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              </div>
              :
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              :
              <>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
              </>
          }     
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>


		</>

		)
}