import React from 'react';
import { Jumbotron, Button } from 'react-bootstrap';
import AppNavBar from './AppNavBar'
import TextTransition, { presets } from "react-text-transition";
import { fadeIn,animations } from 'react-animation'
import {Link} from 'react-router-dom';
import './homebanner.css'

const TEXTS = [
  "Welcome to",
  "The J Shop"
];

const style = {
  animation: animations.popIn
}

export default function HomeBanner({bannerProp}){

 const [index, setIndex] = React.useState(0);

  React.useEffect(() => {
    const intervalId = setInterval(() =>
      setIndex(index => index + 1),
      1500 // every 3 seconds
    );
    return () => clearTimeout(intervalId);
  }, []);

  return (
    <div>
      <Jumbotron className="main">
      <div className ="container">
        <h1 className="content text-center">
      <TextTransition
        text={ TEXTS[index % TEXTS.length] }
        springConfig={ presets.wobbly }
      />
    </h1>
        <p className="lead">{bannerProp.title}</p>
        <hr className="my-2" />
        <p>{bannerProp.description}</p>
        <Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
        </div>
      </Jumbotron>
    </div>
  );
};
