import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import {AnimatePresence, motion} from 'framer-motion'
// import sweetalert2
import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Redirect,useHistory} from 'react-router-dom';

import '../Register.css'

export default function Register(){
const {user,setUser} = useContext(UserContext);

// useHistory allows us to use methods which will redirect a user to  another page without having to reload.

	const history = useHistory();

	// input states
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword,setConfirmPassword] = useState("");

	// conditional rendering for button
	const [isActive,setIsActive] = useState(false)


	/*
		Two Way Binding
		
		In Reactjs, we are able to create form which will allow us to bind the value of our input as the value for our states. We cannot type into our inputs anymore because there is now a value bound to it. We will then add an Onchange event per input to be able to update the state with the current value of the input.

		Two Way Binding so that we can be ensure that we can save the value of our input in our state, So that we can capture the current value of the input as it is typed on and save in a state as opposed to saving it when we are about to submit the values.

		<Form.Control type="inputType" value={inputState}  onChange= {e => {setInputState(e.target.value)}}  />

		e = event, all event listeners pass the event object to the function added in the event listener.

		e.target = target is the element WHERE the event happened.

		e.target.value = value is a property of target. It is the current value of the element WHERE the event happened.
	*/
	// console.log(firstName)
	// console.log(lastName)
	// console.log(email)
	// console.log(mobileNo)
	// console.log(password)
	// console.log(confirmPassword)

	// Validate our form as our user is typing in and disable the button while at least one of the field are empty, or when the mobileNo is less than 11 digits and if the password doesn't match

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) &&(mobileNo.length === 11)){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){
		// prevent submit events default behaviour
		e.preventDefault();

		// console.log(firstName)
		// console.log(lastName)
		// console.log(email)
		// console.log(mobileNo)
		// console.log(password)
		// console.log(confirmPassword)

		fetch('https://secure-cove-16853.herokuapp.com/users/register',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data =>{

			console.log(data)
			// email property will only be not undefined if we registered properly
			if(data.user){

				Swal.fire({
					icon: "success",
					title: "Registration Successful",
					text: data.message
				})
				// redirect our user after registering to our login page.
				// push() method is from  our useHistory which allows us to redirect to another page.
				history.push('./login')

			} else {
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			}
		})
	}

	return (
		
	user.id
			?
			<Redirect to="/" />
			:
	
	<motion.div initial={{ scaleX:0 }} animate={{ scaleX:1 }} exit={{ scaleX: 0 }} transition={{duration: 0.5}}>
		<h1 className="my-5 text-center text-info registerClass">Register</h1>
			<Form onSubmit ={e => registerUser(e)}>
					<Form.Group>
						<input
						className="fNameInput"
						type="text" 
						value={firstName} 
						onChange={e =>{setFirstName(e.target.value)}} 
						required/>
						<Form.Label className="fNameLabel">First Name:</Form.Label>
					</Form.Group>
					<Form.Group>
						<input
						className="lNameInput"
						type="text" 
						value={lastName} 
						onChange={e =>{setLastName(e.target.value)}} 
						required/>
						<Form.Label className="lNameLabel">Last Name:</Form.Label>
					</Form.Group>
					<Form.Group>
						<input
						className="regEmailInput"
						type="email" 
						value={email} 
						onChange={e =>{setEmail(e.target.value)}} 
						required/>
						<Form.Label className="regEmailLabel">Email:</Form.Label>
					</Form.Group>
					<Form.Group>
						<input
						className="mobileInput"
						type="number" 
						value={mobileNo} 
						onChange={e =>{setMobileNo(e.target.value)}}  
						required/>
						<Form.Label className="mobileLabel">Mobile No:</Form.Label>
					</Form.Group>
					<Form.Group>
						<input
						className="regPasswordInput" 
						type="password"
						value={password} 
						onChange={e =>{setPassword(e.target.value)}} 
						required/>
						<Form.Label className="regPasswordLabel">Password:</Form.Label>
					</Form.Group>
					<Form.Group>
						<input
						className="confirmPasswordInput" 
							type="password" 
							value={confirmPassword} 
							onChange = {e =>{setConfirmPassword(e.target.value)}} 
							required/>
						<Form.Label className="confirmPasswordLabel">Confirm Password:</Form.Label>
					</Form.Group>
					{
						isActive
						? <Button variant="success" type="submit">Submit</Button>
						: <Button variant="danger" type="submit" disabled>Submit</Button>
					}
					
			</Form>
			
	</motion.div>
		)
	
}