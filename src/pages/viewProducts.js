import React,{useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,useHistory, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import '../App.css'

export default function ViewProduct() {

	// useParams will allow us to retreive any parameter in out URL
	// useParams() will return an object containing our URL params
	// console.log(useParams())
	const {productId} = useParams();
	console.log(productId)
	const{user} = useContext(UserContext);
	//save useHistory and its methods as history variable to use its methods.
	const history = useHistory();
	// save product details
	const [productDetails,setProductDetail] = useState({

		name: null,
		description: null,
		price: null
	})

	// add useEffect({},[]) to get product details:
	useEffect(() =>{

		fetch(`https://secure-cove-16853.herokuapp.com/products/retrieveSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data =>{
			// console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Product Unavailable",
					text: data.message
				})
			} else{
				setProductDetail({

					name: data.name,
					description: data.description,
					createdOn: data.createdOn,
					price: data.price,
					

					})
			}
			// console.log(data)
		})
	},[productId])
	console.log(productId)

	function addToCart (e) {

		
	const storage = JSON.parse(localStorage.getItem('items'))
 const cart = [];
		 if(storage === null)
		 {
		
		 cart.push(productDetails)
		 localStorage.setItem("items",JSON.stringify(cart))
		 } else{
		 
		 	 storage.forEach(check => {
 			console.log (check.name === productDetails.name)
 			if(check.name ===  productDetails.name){
 				alert("Already been added to cart")
 			}  else {
 				 storage.push(productDetails)
		 		 localStorage.setItem("items",JSON.stringify(cart))
 			}
 		})
 }

 		
		 
		 
	}



				
				// redirect our user after registering to our login page.
				// push() method is from  our useHistory which allows us to redirect to another page.
				// history.push('./login')

			 

	
	return (

			<>
				<Row>
					<Col>
						<Card className="setViewProductCard bg-transparent">
							<Card.Body className="bg-transparent text-justify setViewProduct">
								<div className="cardContainer text-center">
								<Card.Title>{productDetails.name}</Card.Title>
								<Card.Text>{productDetails.description}</Card.Text>
								<Card.Text>Price: {productDetails.price}</Card.Text>
								</div>
								{
									user.isAdmin === false
									?
									<Button variant="primary" block onClick={(e) =>(addToCart(
									Swal.fire({
										  title: 'Product added',
										  text: `${productDetails.name} has been added to cart`,
										  imageUrl: 'https://media1.giphy.com/media/fxfpEJw4t64dF7eWMu/giphy.gif?cid=ecf05e47as7bp9w7zbfg07izwewv9ibfgnizjh2el9wnu21u&rid=giphy.gif&ct=g',
										  imageWidth: 400,
										  imageHeight: 200,
										  imageAlt: 'Custom image',
									})
									))}>Add to Cart</Button>
									:
									<Link className="setViewProductCard btn btn-danger btn-block" to="/login">Login to Enroll</Link>
								}
								
							</Card.Body>

						</Card>

					</Col>

				</Row>
				
			</>



		)
}